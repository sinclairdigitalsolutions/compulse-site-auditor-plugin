<div>
    <form action="options.php" method="post">
        <?php

        settings_fields('compulse_site_auditor');
        do_settings_sections('compulse_site_auditor');
        submit_button("Save");

        ?>
    </form>
</div>

<div>
    <?php

    $num_sites = count( $this->get_site_list_array() );

    ?>
    <h2>Cron Status</h2>
    <div><b>Auditor Next Index:</b> <?php print get_option('compulse_site_auditor_next_index', 0); ?></div>
    <div><b>Auditor Number of Sites:</b> <?php print $num_sites; ?></div>
    <div><b>Auditor Sites Per Run:</b> <?php print $this->get_sites_per_cron_run( $num_sites ); ?></div>
    <div><b>Time to Audit All Sites:</b> <?php

        if ( $this->get_sites_per_cron_run( $num_sites ) > 0 ) {
            echo round( ( $num_sites * ( 5.00 / $this->get_sites_per_cron_run( $num_sites ) ) ) / ( 60 * 24 ), 2 ) . ' days';
        } else {
            echo 'Until the end of time (Sites Per Run is 0)';
        }

    ?></div>
</div>

<div>
    <h2>Send Report</h2>
    <div>Send the current site issue report to the specified recipients.</div>
    <div>
        <form action="#" id="send-report-form">
            <div>
                <label>
                    <div>Recipients (comma separated):</div>
                    <div style="margin:10px 0px;">
                        <input type="text" id="send-report-recipients" placeholder="Email recipients" style="width:100%;" />
                    </div>
                </label>
            </div>
            <div>
                <input type="submit" name="submit" id="submit" class="button button-primary" value="Send Report Now" />
                <input type="button" name="preview" id="preview-audit-report" class="button button-primary" value="Preview Report" />
                <input type="button" name="hide-preview" id="hide-audit-report" class="button button-primary" value="Hide Preview" style="display:none;" />
            </div>
        </form>
    </div>
    <div class="auditor-report-body"></div>
</div>

<div>
    <h2>Extract Installation Names</h2>
    <div>
        <form action="#" id="install-table-extract-form">
            <div>
                <textarea placeholder="Paste WP Engine's install list HTML here, starting with <div class='sites-tree'>..." style="width:100%; height:300px;"></textarea>
            </div>
            <div>
                <input type="submit" name="submit" id="submit" class="button button-primary" value="Extract Install Names" />
            </div>
        </form>
    </div>
</div>

<!--<div>This is just for testing. Will be removed:</div>
<div style="padding:10px;">
    <?php //print do_shortcode('[auditor_table /]'); ?>
</div>-->

<script>
    (function($) {
        // Add install name extractor script.
        $("#install-table-extract-form").submit(function(e) {
            var textarea = $(this).find('textarea');
            var val = textarea.val();
            var tree = $(val);

            var installList = [];

            var selectors = [
                '.panel.multi-env-site .panel-footer .col.install-name a:first-child', // multi-environment sites
                '.panel:not(.multi-env-site) .panel-body .col.site-name a:first-child' // legacy single install
            ];

            tree.find( selectors.join(',') ).each(function() {
                installList.push($(this).html());
            });

            textarea.val(installList.join(','));

            e.preventDefault();
            return false;
        });

        $("#send-report-form").submit(function(e) {
            var recipients = $(this).find("#send-report-recipients").val();
            var btn = $(this).find("#submit");

            var originalVal = btn.val();
            btn.val('Sending...').prop('disabled', true);

            $.post('/wp-admin/admin-ajax.php', {
                'action': 'compulse_send_report_now',
                'recipients': recipients
            }, function(data) {
                console.log(data);

                var message = $('<div />');

                if (data.success) {
                    message.html('Successfully sent.');
                } else {
                    message.html('Failed to send');
                }

                btn.after(message);
                setTimeout(function() {
                    message.remove();
                }, 10000);

                btn.val(originalVal).prop('disabled', false);
            });

            e.preventDefault();
            return false;
        });

        $("#preview-audit-report").click(function(e) {
            var btn = $(this);

            var originalVal = btn.val();
            btn.val('Generating preview...').prop('disabled', true);

            $.post('/wp-admin/admin-ajax.php', {
                'action': 'compulse_preview_report'
            }, function(data) {
                $(".auditor-report-body").html(data.report);
                btn.val(originalVal).prop('disabled', false);
                $("#hide-audit-report").show();
            });
        });

        $("#hide-audit-report").click(function(e) {
            $(this).hide();
            $(".auditor-report-body").empty();
        });
    })(jQuery);
</script>
