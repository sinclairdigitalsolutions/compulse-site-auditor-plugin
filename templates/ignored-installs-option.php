<?php

$manager = SiteAuditTestManager::get_instance();
$test_names = $manager->get_all_test_names();

$value = get_option('compulse_ignored_sites', []);
$value = is_array($value) ? array_values($value) : [];

?>

<div style="margin-bottom:5px;">
    Enter the installs/tests that should be <i>excluded</i> from the daily report email.
</div>
<table class="ignore-installs-table"
    data-test-names="<?php echo esc_attr( json_encode($test_names) ); ?>"
    data-value="<?php echo esc_attr( json_encode( $value ) ); ?>">

    <thead>
        <tr>
            <th>Install Name</th>
            <?php foreach ( $test_names as $test_name ): ?>
                <th><?php print $test_name; ?> test</th>
            <?php endforeach; ?>
            <th>Note</th>
            <th></th>
        </tr>
    </thead>

    <tbody>

    </tbody>
</table>

<script type="text/html" id="ignored-sites-row-template">
    <tr>
        <td><input type="text" name="" class="ignored-site-install" /></td>
        <td class="selects"></td>
        <td><input type="text" name="" class="ignored-site-note" /></td>
        <td>
            <a href="#" class="ignore-installs-remove-link">Remove</a>
        </td>
    </tr>
</script>

<script type="text/html" id="ignored-sites-select-template">
    <td>
        <select name="" class="ignored-site-test">
            <option value="">Do Not Ignore</option>
            <option value="error">Ignore Errors</option>
            <option value="warning">Ignore Warnings</option>
            <option value="error,warning">Ignore Errors and Warnings</option>
        </select>
    </td>
</script>

<div>
    <a href="#" id="ignore-installs-add-row">+ Add Row</a>
</div>

<script>
    (function($) {
        var table = $(".ignore-installs-table");
        var tableBody = table.find('tbody');

        var value = table.data('value');
        var testNames = table.data('test-names');

        console.log(testNames);

        var rowTemplate = $("#ignored-sites-row-template").html();
        var selectTemplate = $("#ignored-sites-select-template").html();

        var nextIndex;

        function addRow(data, index) {
            var row = $( rowTemplate );
            row.find('.ignored-site-install').attr('name', 'compulse_ignored_sites[' + index + '][install]');
            row.find('.ignored-site-note').attr('name', 'compulse_ignored_sites[' + index + '][note]');

            if ( data.install ) {
                row.find('.ignored-site-install').val(data.install);
            }

            if ( data.note ) {
                row.find('.ignored-site-note').val(data.note);
            }

            var selectContainer = $('<tr />');

            for ( var i in testNames ) {
                var select = $( selectTemplate );

                select.find('select').attr('name', 'compulse_ignored_sites[' + index + '][' + testNames[i] + ']');
                if ( data[ testNames[i] ] ) {
                    select.find('select').val( data[ testNames[i] ] );
                }

                selectContainer.append( select );
            }

            row.find("td.selects").replaceWith( selectContainer.find('td') );

            tableBody.append(row);
        }

        $("#ignore-installs-add-row").click(function(e) {
            addRow( {}, nextIndex );
            nextIndex++;

            e.preventDefault();
            return false;
        });

        $("table.ignore-installs-table").on('click', '.ignore-installs-remove-link', function(e) {
            $(this).closest('tr').remove();
            e.preventDefault();
            return false;
        });

        // Add initial values.

        for ( var i in value ) {
            addRow( value[i], i );
        }

        nextIndex = tableBody.find('tr').length;
    })(jQuery);
</script>

<style>
    .ignore-installs-table {
        width: 100%;
    }

    .ignore-installs-table tr th, table.ignore-installs-table tr td {
        text-align: center;
    }

    .ignore-installs-table select {
        width: 120px;
    }
</style>
