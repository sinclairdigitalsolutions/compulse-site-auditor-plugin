<?php

global $wpdb;

$site_data_by_server = [
    'sinclair0/sinclair2' => [
        'total_installs' => 0,
        'total_disk_usage' => 0,
        'uploads_disk_usage' => 0
    ],
    'sinclair1' => [
        'total_installs' => 0,
        'total_disk_usage' => 0,
        'uploads_disk_usage' => 0
    ],
    'sinclair3' => [
        'total_installs' => 0,
        'total_disk_usage' => 0,
        'uploads_disk_usage' => 0
    ],
    'sinclair4' => [
        'total_installs' => 0,
        'total_disk_usage' => 0,
        'uploads_disk_usage' => 0
    ],
    'sinclair5' => [
        'total_installs' => 0,
        'total_disk_usage' => 0,
        'uploads_disk_usage' => 0
    ]
];

$query = "
    SELECT
        server.meta_value AS server,
        COUNT(*) AS count
    FROM wp_posts
    LEFT JOIN wp_postmeta AS server ON (wp_posts.ID=server.post_id AND server.meta_key='server')
    WHERE wp_posts.post_type='auditor_result'
    AND server.meta_value IS NOT NULL
    GROUP BY server.meta_value
";

$total_installs = $wpdb->get_results( $query, ARRAY_A );

foreach ( $total_installs as $total ) {
    $site_data_by_server[ $total['server'] ]['total_installs'] = intval($total['count']);
}

$query = "
    SELECT
        server.meta_value AS server,
        SUM(CAST(disk_total.meta_value AS UNSIGNED)) AS total_sum,
        SUM(CAST(disk_uploads.meta_value AS UNSIGNED)) AS uploads_sum
    FROM wp_posts
    LEFT JOIN wp_postmeta AS server ON (wp_posts.ID=server.post_id AND server.meta_key='server')
    LEFT JOIN wp_postmeta AS disk_total ON (server.post_id=disk_total.post_id AND disk_total.meta_key='disk_total')
    LEFT JOIN wp_postmeta AS disk_uploads ON (server.post_id=disk_uploads.post_id AND disk_uploads.meta_key='disk_uploads')
    WHERE wp_posts.post_type='auditor_result'
    AND disk_total.meta_value IS NOT NULL
    GROUP BY server.meta_value
";

$total_disk_usage = $wpdb->get_results( $query, ARRAY_A );

foreach ( $total_disk_usage as $total ) {
    $site_data_by_server[ $total['server'] ]['total_disk_usage'] = intval($total['total_sum']);
    $site_data_by_server[ $total['server'] ]['uploads_disk_usage'] = intval($total['uploads_sum']);
}


/////

foreach ( $site_data_by_server as $server => $data ): ?>
    <?php

        if ( empty( $server ) ) {
            continue;
        }

    ?>

    <div style="margin:20px;">
        <h2><?php echo $server; ?></h2>
        <div>
            <b>Total Installs:</b> <?php echo ($data['total_installs'] == 0) ? 'Unknown' : number_format( $data['total_installs'] ); ?>
        </div>
        <?php if ( isset( $data['total_disk_usage'] ) ): ?>
            <div>
                <b>Total Disk Usage:</b> <?php echo ($data['total_disk_usage'] == 0) ? 'Unknown' : SiteAuditorUtils::format_kb( $data['total_disk_usage'] ); ?>
            </div>
        <?php endif; ?>
        <?php if ( isset( $data['uploads_disk_usage'] ) ): ?>
            <div>
                <b>Uploads Disk Usage:</b> <?php echo ($data['uploads_disk_usage'] == 0) ? 'Unknown' : SiteAuditorUtils::format_kb( $data['uploads_disk_usage'] ); ?>
            </div>
        <?php endif; ?>
    </div>
<?php endforeach; ?>

<div style="margin:20px; margin-top:50px;">
    <h1>Plugins</h1>

    <?php

    $query = "
        SELECT
            wp_postmeta.meta_value AS plugin_name,
            COUNT(*) AS count
        FROM wp_postmeta
        WHERE wp_postmeta.meta_key='active_plugin_names'
        AND wp_postmeta.meta_value != ''
        GROUP BY wp_postmeta.meta_value
        ORDER BY wp_postmeta.meta_value ASC
    ";

    $plugins = $wpdb->get_results( $query, ARRAY_A );

    ?>

    <table>
        <thead>
            <tr>
                <th>Plugin Name</th>
                <th># Active Launched Sites</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $plugins as $plugin ): ?>
                <tr>
                    <td><?php echo $plugin['plugin_name']; ?></td>
                    <td><?php echo number_format( $plugin['count'] ); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
