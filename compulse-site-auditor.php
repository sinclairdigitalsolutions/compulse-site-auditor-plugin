<?php

/**
 * Plugin Name: Compulse Site Auditor
 * Description: Periodically checks Compulse's WP Engine websites for issues.
 * Version: 1.14.2
 * Author: Kevin Hall, Compulse
 * Author URI: https://compulse.com
 */

// ini_set('display_errors', '1');
// error_reporting(E_ALL);

include "audit.php";
include 'include/action-scheduler/action-scheduler.php';

define('COMPULSE_SITE_AUDITOR_TEMPLATE_PATH', dirname(__FILE__) . '/templates/');

/**
 * Set up the WordPress plugin.
 */
class CompulseSiteAuditorPlugin {
    /**
     * Cached list of sites to audit (comma-separated)
     * @var string
     */
    private $site_list;

    /**
     * Hook to execute when the main cron runs.
     * @var string
     */
    private $cron_name;

    /**
     * Hook to execute when the daily report cron runs.
     * @var string
     */
    private $daily_report_cron_name;

    /**
     * The full path to this plugin's directory.
     * @var string
     */
    private $plugin_dir_path;

    /**
     * Ignored sites indexed by install name.
     * @var array
     */
    private $ignored_sites;

    /**
     * Set up data and add hooks for the WP plugin.
     */
    public function __construct() {
        $this->site_list = [];
        $this->cron_name = 'compulse_site_auditor_cron';
        $this->daily_report_cron_name = 'daily_report_email_cron';
        $this->plugin_dir_path = dirname(__FILE__);

        $this->ignored_sites = [];

        add_action('init', array($this, 'init'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('admin_init', array($this, 'register_settings'));
        add_action('admin_menu', array($this, 'admin_menu'));
        add_filter('cron_request', array($this, 'cron_request'));
        add_filter('cron_schedules', array($this, 'cron_schedules'));

        add_action($this->cron_name, array($this, 'cron'));
        add_action($this->daily_report_cron_name, array($this, 'daily_audit_report_cron'));

        add_action('updated_option', array($this, 'updated_site_list_option'), 10, 3);

        add_action('wp_ajax_compulse_run_audit_now', array($this, 'run_audit_now_ajax'));
        add_action('wp_ajax_nopriv_compulse_run_audit_now', array($this, 'run_audit_now_ajax'));

        add_action('wp_ajax_compulse_send_report_now', array($this, 'send_report_now_ajax'));
        add_action('wp_ajax_compulse_preview_report', array($this, 'preview_report_ajax'));

        add_shortcode('auditor_table', array($this, 'auditor_table_shortcode'));
        add_shortcode( 'auditor_statistics', array($this, 'auditor_statistics_shortcode') );

        $this->load_site_list();

        // schedule cron, if not scheduled.
        if ( !wp_next_scheduled($this->cron_name) ) {
            wp_schedule_event(time(), 'compulse_site_auditor_schedule', $this->cron_name);
        }

        if ( !wp_next_scheduled('daily_report_email_cron') ) {
            wp_schedule_event(time(), 'hourly', 'daily_report_email_cron');
        }
    }

    /**
     * Load the comma-separated site list from the database and return it.
     * @return string
     */
    private function load_site_list() {
        $this->site_list = get_option('compulse_site_list');
        return $this->site_list;
    }

    /**
     * Register the auditor_result post type.
     * Hooked to init action.
     */
    public function init() {
        register_post_type('auditor_result', [
            'labels' => [
                'name' => 'Auditor Results',
                'singular_name' => 'Auditor Result'
            ],
            'public' => false,
            'show_ui' => false,
            'show_in_menu' => false,
            'capability_type' => 'post',
            'capabilities' => [
                'create_posts' => false
            ],
            'map_meta_cap' => true
        ]);

        // if ( !as_next_scheduled_action( $this->cron_name ) ) {
        //     as_schedule_recurring_action( time(), 60 * 5, $this->cron_name, [], 'compulse_site_auditor' );
        // }
        //
        // if ( !as_next_scheduled_action( $this->daily_report_cron_name ) ) {
        //     as_schedule_recurring_action( time(), DAY_IN_SECONDS, $this->daily_report_cron_name, [], 'compulse_site_auditor' );
        // }

        //$this->send_audit_report('krhall@compulse.com');
    }

    /**
     * Hooked to wp_enqueue_scripts action.
     */
    public function enqueue_scripts() {
        // Make sure jQuery is enqueued.
        wp_enqueue_script('jquery');
    }

    /**
     * Remove the port number from local requests.
     * Hooked to cron_request filter.
     * @param array $request
     * @return array
     */
    public function cron_request($request) {
        $matches = [];

        if ( preg_match( '#^https?://(localhost:[1-9][0-9]+)#', $request['url'], $matches ) ) {
            $request['url'] = str_replace( $matches[1], 'localhost', $request['url'] );
        }

        return $request;
    }

    /**
     * Add the auditor's cron schedule (every 5 minutes).
     * Hooked to cron_schedules filter.
     * @param array $schedules
     * @return array
     */
    public function cron_schedules($schedules = array()) {
        $schedules['compulse_site_auditor_schedule'] = array(
            'interval' => 5 * 60, // 5 minutes
            'display' => 'Site Auditor (Every 5 Minutes)'
        );

        return $schedules;
    }

    /**
     * Get the site list as an array.
     * @return string[]
     */
    public function get_site_list_array() {
        // cache this.
        $site_list_array = explode(',', $this->site_list);
        $site_list_array = array_map('trim', $site_list_array);

        return $site_list_array;
    }

    /**
     * Retrieve and cache the list of ignored sites, keyed by install name.
     * @return array
     */
    public function get_ignored_sites_array() {
        if ( empty($this->ignored_sites) ) {
            $ignored_sites = get_option('compulse_ignored_sites', []);
            $ignored_sites = is_array($ignored_sites) ? array_values($ignored_sites) : [];

            foreach ( $ignored_sites as $k => $ignored_site ) {
                $ignored_sites[ $ignored_site['install'] ] = $ignored_site;
                unset($ignored_sites[$k]);
            }

            $this->ignored_sites = $ignored_sites;
        }

        return $this->ignored_sites;
    }

    /**
     * Determine the number of sites that should be audited during each cron run.
     * @param int $num_sites The total number of sites.
     * @return int
     */
    public function get_sites_per_cron_run( $num_sites ) {
        // $schedules = $this->cron_schedules();
        // $auditor_schedule_interval = $schedules['compulse_site_auditor_schedule']['interval'];
        //
        // $runs_per_day = (24 * 60) / ($auditor_schedule_interval / 60);
        // $sites_per_run = ceil( $num_sites / ( $runs_per_day ) );
        //
        // return $sites_per_run;

        $sites_per_run = (int) get_option('compulse_sites_per_cron_run', 10);

        return $sites_per_run;
    }

    /**
     * Handle an ajax request to run an audit now for an install.
     * Hooked to wp_ajax_compulse_run_audit_now/wp_ajax_nopriv_compulse_run_audit_now actions.
     */
    public function run_audit_now_ajax() {
        $install_name = filter_input(INPUT_POST, 'install_name', FILTER_DEFAULT);

        if ( !empty($install_name) && in_array($install_name, $this->get_site_list_array()) ) {
            $result_id = $this->run_auditor($install_name);

            if ($result_id > 0) {
                wp_send_json([
                    'row_html' => $this->get_auditor_table_row($result_id),
                    'success' => true
                ]);
                exit;
            }
        }

        wp_send_json([
            'success' => false
        ]);
        exit;
    }

    /**
     * Handle an ajax request to send the daily report now.
     * Hooked to wp_ajax_compulse_send_report_now action.
     */
    public function send_report_now_ajax() {
        $recipients = filter_input(INPUT_POST, 'recipients', FILTER_DEFAULT);

        if ( !empty($recipients) ) {
            $success = $this->send_audit_report($recipients);

            wp_send_json([
                'success' => $success
            ]);
            exit;
        }

        wp_send_json([
            'success' => false
        ]);
        exit;
    }

    /**
     * Handle an ajax request to preview the daily report.
     * Hooked to wp_ajax_compulse_preview_report.
     */
    public function preview_report_ajax() {
        wp_send_json([
            'report' => nl2br( $this->get_audit_report_body() )
        ]);
        exit;
    }

    /**
     * The main site auditor cron event.  Called when the cron executes.
     * Runs tests for the next set of sites.
     * Hooked to compulse_site_auditor_cron action.
     */
    public function cron() {
        // Called when cron executes.
        $site_list_array = $this->get_site_list_array();
        $num_sites = count($site_list_array);

        if ($num_sites > 0) {
            $next_index = get_option('compulse_site_auditor_next_index', 0);

            if ( $next_index >= $num_sites ) {
                $next_index = 0;
            }

            // this runs every 5 minutes.
            // goal is to run every site every other day?
            $sites_per_run = $this->get_sites_per_cron_run($num_sites);

            // Figure out which set of audits to run during this cron request.
            $site_list_this_run = array_slice($site_list_array, $next_index, $sites_per_run);

            foreach ($site_list_this_run as $site) {
                $this->run_auditor($site);
            }

            //error_log($next_index . ' ' . $sites_per_run);

            update_option('compulse_site_auditor_next_index', $next_index + $sites_per_run);
        }
    }

    /**
     * Set up the admin menu.
     * Hooked to admin_menu action.
     */
    public function admin_menu() {
        add_options_page( "Compulse Site Auditor", "Compulse Site Auditor", "manage_options", "compulse_site_auditor", array($this, 'settings_page') );
    }

    /**
     * Register settings for the auditor settings page.
     * Hooked to admin_init action.
     */
    public function register_settings() {
        register_setting('compulse_site_auditor', 'compulse_site_list');
        register_setting('compulse_site_auditor', 'compulse_ignored_sites');
        register_setting('compulse_site_auditor', 'compulse_sites_per_cron_run');
        register_setting('compulse_site_auditor', 'compulse_daily_report_emails');
        register_setting('compulse_site_auditor', 'compulse_daily_error_report_emails');
        add_settings_section('compulse_site_auditor', 'Compulse Site Auditor Settings', '__return_empty_string', 'compulse_site_auditor');
    	add_settings_field("compulse_site_list", "Site List", array($this, 'site_list_option'), "compulse_site_auditor", "compulse_site_auditor");
        add_settings_field("compulse_ignored_sites", "Ignored Installs", array($this, 'ignored_installs_option'), "compulse_site_auditor", "compulse_site_auditor");
        add_settings_field('compulse_sites_per_cron_run', "# Sites per Cron Run", array($this, 'sites_per_cron_run_option'), 'compulse_site_auditor', 'compulse_site_auditor');
        add_settings_field('compulse_daily_report_emails', 'Daily Report Emails', array($this, 'daily_report_emails_option'), 'compulse_site_auditor', 'compulse_site_auditor');
        add_settings_field( 'compulse_daily_error_report_emails', 'Daily ERROR Report Emails', array( $this, 'daily_error_report_emails_option' ), 'compulse_site_auditor', 'compulse_site_auditor' );
    }

    /**
     * Process the new site list when it is updated.
     * Delete any auditor results for sites that are no longer in the list.
     * Hooked to updated_option action.
     * @param string $option_name
     * @param mixed $old_value
     * @param mixed $new_value
     */
    public function updated_site_list_option($option_name, $old_value, $new_value) {
        if ( 'compulse_site_list' != $option_name ) {
            return;
        }

        $this->load_site_list();

        $site_list = $this->get_site_list_array();

        $results = get_posts([
            'post_type' => 'auditor_result',
            'posts_per_page' => -1,
            'post_status' => 'private',
            'order' => 'asc',
            'orderby' => 'title'
        ]);

        // Delete any auditor results for sites that are no longer in the list.
        foreach ( $results as $result ) {
            if ( !in_array($result->post_title, $site_list) ) {
                wp_delete_post($result->ID);
            }
        }
    }

    /**
     * Display the ignored installs setting.
     */
    public function ignored_installs_option() {
        include COMPULSE_SITE_AUDITOR_TEMPLATE_PATH . 'ignored-installs-option.php';
    }

    /**
     * Display the site list setting.
     */
    public function site_list_option() {
        include COMPULSE_SITE_AUDITOR_TEMPLATE_PATH . 'site-list-option.php';
    }

    /**
     * Helper to display a text field setting.
     * @param string $name
     * @param string $description
     */
    private function text_field_option($name, $description = "") {
        print '<div><input type="text" name="' . $name . '" style="width:100%;" value="' . get_option($name, '') . '" /></div>';
        print '<div>' . $description . '</div>';
    }

    /**
     * Display the sites per cron run setting.
     */
    public function sites_per_cron_run_option() {
        $this->text_field_option('compulse_sites_per_cron_run', 'How many sites should be audited each time the cron runs?');
    }

    /**
     * Display the daily report emails settings.
     */
    public function daily_report_emails_option() {
        $this->text_field_option('compulse_daily_report_emails', 'Comma-separated list of emails that will receive the daily report.');
    }

    public function daily_error_report_emails_option() {
        $this->text_field_option('compulse_daily_error_report_emails', 'Comma-separated list of emails that will receive the daily ERROR-only report.');
    }

    /**
     * Display the settings page.
     */
    public function settings_page() {
        include COMPULSE_SITE_AUDITOR_TEMPLATE_PATH . 'settings-page.php';
    }








    /**
     * Runs every hour.
     */
    public function daily_audit_report_cron() {
        $now = new DateTime( 'now', new DateTimeZone( 'America/New_York' ) );
        $hour = (int) $now->format( 'G' );

        if ( $hour == 7 ) {
            $to_email = get_option( 'compulse_daily_report_emails', 'krhall@compulse.com' );
            $this->send_audit_report( $to_email );
        }

        if ( in_array( $hour, [ 7, 12, 17 ] ) ) {
            $errors_to_email = get_option( 'compulse_daily_error_report_emails', 'krhall@compulse.com' );
            $this->send_audit_report( $errors_to_email, true );
        }
    }

    /**
     * Retrieve the body for the daily report email.
     * @return string
     */
    public function get_audit_report_body( $errors_only = false ) {
        $results = get_posts([
            'post_type' => 'auditor_result',
            'posts_per_page' => -1,
            'post_status' => 'private',
            'order' => 'asc',
            'orderby' => 'title'
        ]);

        $ignored_sites = $this->get_ignored_sites_array();

        $message  = "<h1>Compulse Site Auditor Report</h1>";
        $message .= "To view all auditor results or rerun a site audit, go to <a href=\"https://krhall2.wpengine.com/auditor-results/\" target=\"_blank\">https://krhall2.wpengine.com/auditor-results/</a><br /><br />";

        $sites_with_errors = [];
        $sites_with_warnings = [];

        foreach ($results as $result) {
            $result_data = get_post_meta($result->ID, 'results', true);

            foreach ( $result_data as $test => $data ) {
                $ignored_statuses_for_test = [];

                if ( array_key_exists($result->post_title, $ignored_sites) ) {
                    $ignored_statuses_for_test = explode(',', $ignored_sites[ $result->post_title ][ $test ]);
                }

                //var_dump($ignored_statuses_for_test); //exit;

                if ( $data['status'] == 'error' && !in_array('error', $ignored_statuses_for_test) ) {
                    $sites_with_errors[] = [
                        'id' => $result->ID,
                        'result_data' => $result_data,
                        'meta' => get_post_meta($result->ID)
                    ];

                    break;
                } elseif ( $data['status'] == 'warning' && !in_array('warning', $ignored_statuses_for_test) ) {
                    $sites_with_warnings[] = [
                        'id' => $result->ID,
                        'result_data' => $result_data,
                        'meta' => get_post_meta($result->ID)
                    ];
                    break;
                }
            }
        }

        if ( empty( $sites_with_errors ) && $errors_only ) {
            // Don't send the error email if there are no errors.
            return '';
        }

        if ( !empty($sites_with_errors) ) {
            $message .= '<h3>The following WP Engine installs have <font color="#cc0000">errors</font>:</h3>';
            foreach ( $sites_with_errors as $site ) {
                $display_domain = (strlen( $site['meta']['domain'][0] ) > 50) ? substr( $site['meta']['domain'][0], 0, 50 ) . '...' : $site['meta']['domain'][0];
                $message .= '<b>' . $display_domain . '</b> <i>(' . $site['meta']['install_name'][0] . ' on ' . $site['meta']['server'][0] . ')</i><br />';

                $message .= '<small>';
                    $message .= '<a href="' . $site['meta']['domain'][0] . '" target="_blank">View Site</a>&nbsp;';
                    $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    $message .= '<a href="https://my.wpengine.com/installs/' . $site['meta']['install_name'][0] . '" target="_blank">View Install</a>';
                    $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                    $matches = [];
                    if ( preg_match( '#^https?://([^/]+)/?#', $site['meta']['domain'][0], $matches ) ) {
                        $message .= '<a href="https://www.ultratools.com/tools/dnsLookupResult?domain=' . $matches[1] . '" target="_blank">View DNS Records</a>';
                        $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }

                    $message .= '<a href="https://krhall2.wpengine.com/auditor-results/?search=' . $site['meta']['install_name'][0] . '" target="_blank">View on Auditor</a>';
                $message .= '</small>';
                $message .= '<br />';

                $error_string = '';

                foreach ( $site['result_data'] as $result ) {
                    if ( $result['status'] == 'error' || $result['status'] == 'warning' ) {
                        foreach ( $result['messages'] as $current_message ) {
                            if ( $current_message['status'] == 'error' || $current_message['status'] == 'warning' ) {
                                $error_string .= '<li><b>' . $current_message['status'] . ':</b> ' . $current_message['message'] . '</li>';
                            }
                        }
                    }
                }

                if ( !empty( $error_string ) ) {
                    $message .= '<ul>' . $error_string . '</ul>';
                }

                $message .= '<br />';
            }
        } else {
            $message .= 'No sites have errors.<br />';
        }

        $message .= '<br /><br /><br />';

        if ( !$errors_only ) {
            if ( !empty( $sites_with_warnings ) ) {
                $message .= '<h3>The following WP Engine installs have <font color="#cccc00">warnings</font>:</h3>';
                foreach ( $sites_with_warnings as $site ) {
                    $display_domain = ( strlen( $site['meta']['domain'][0] ) > 50 ) ? substr( $site['meta']['domain'][0], 0, 50 ) . '...' : $site['meta']['domain'][0];
                    $message .= '<b>' . $display_domain . '</b> <i>(' . $site['meta']['install_name'][0] . ' on ' . $site['meta']['server'][0] . ')</i><br />';

                    $message .= '<small>';
                    $message .= '<a href="' . $site['meta']['domain'][0] . '" target="_blank">View Site</a>&nbsp;';
                    $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    $message .= '<a href="https://my.wpengine.com/installs/' . $site['meta']['install_name'][0] . '" target="_blank">View Install</a>';
                    $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

                    $matches = [];
                    if ( preg_match( '#^https?://([^/]+)/?#', $site['meta']['domain'][0], $matches ) ) {
                        $message .= '<a href="https://www.ultratools.com/tools/dnsLookupResult?domain=' . $matches[1] . '" target="_blank">View DNS Records</a>';
                        $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    }

                    $message .= '<a href="https://krhall2.wpengine.com/auditor-results/?search=' . $site['meta']['install_name'][0] . '" target="_blank">View on Auditor</a>';
                    $message .= '</small>';
                    $message .= '<br />';

                    $error_string = '';

                    foreach ( $site['result_data'] as $result ) {
                        if ( $result['status'] == 'warning' ) {
                            foreach ( $result['messages'] as $current_message ) {
                                if ( $current_message['status'] == 'warning' ) {
                                    $error_string .= '<li><b>' . $current_message['status'] . ':</b> ' . $current_message['message'] . '</li>';
                                }
                            }
                        }
                    }

                    if ( !empty( $error_string ) ) {
                        $message .= '<ul>' . $error_string . '</ul>';
                    }

                    $message .= '<br />';
                }
            } else {
                $message .= 'No sites have warnings.<br />';
            }
        }

        if ( !$errors_only ) {
            $message .= '<br /><br />Note - The following installs were ignored in this report:<br />';
            foreach ( $ignored_sites as $ignored_site ) {
                $ignore_strings = [];

                foreach ( [ 'enabled', 'launched', 'search', 'domain', 'analytics', 'core', 'ssl' ] as $key ) {
                    if ( !empty( $ignored_site[ $key ] ) ) {
                        $ignore_strings[] = $key;
                    }
                }

                if ( !empty( $ignore_strings ) ) {
                    $message .= '<a href="https://my.wpengine.com/installs/' . $ignored_site['install'] . '" target="_blank">' . $ignored_site['install'] . '</a> is ignoring: ' . implode( ', ', $ignore_strings ) . ' (Note: ' . $ignored_site['note'] . ')' . '<br />';
                }
            }
        }

        return $message;
    }

    /**
     * Send the audit report email.
     * @param string|array $to_email Who the email should be sent to.
     * @return bool The result of wp_mail()
     */
    public function send_audit_report($to_email, $errors_only = false) {
        $message = $this->get_audit_report_body( $errors_only );

        if ( $message ) {
            return wp_mail( $to_email, $errors_only ? 'Compulse Site Auditor Error Report' : 'Daily Compulse Site Auditor Full Report', $message, [
                'Content-Type: text/html; charset=utf-8',
                'Reply-To: krhall@compulse.com'
            ] );
        } else {
            return false;
        }
    }

    /**
     * Set up and run the auditor on an install.
     * When the audit is done, determine additional data and save everything to an auditor_result post.
     * @param string $site The site to audit.
     * @param array|bool $tests An array of tests to run or true to run all tests.
     * @return int The post ID of the result post.
     */
    public function run_auditor($site, $tests = true) {
        $auditor = new SiteAuditor($site);

        set_time_limit( 10 * 60 );

        if ( is_array($tests) ) {

        } elseif ( $tests === true ) {
            $auditor->run_all_tests();
        }

        $result_arrays = [];
        $all_results = $auditor->get_all_test_results();

        foreach ( $all_results as $k => $result ) {
            if ( !empty($result) ) {
                $result_arrays[ $k ] = $result->to_array();
            } else {
                $result_arrays[ $k ] = $result;
            }
        }

        $site_data = $auditor->get_site_data();
        $first_site_data = $site_data[0];
        $last_site_data = $site_data[ count($site_data) - 1 ];

        $ip_to_server = [
            // Old servers
            '104.199.123.216' => 'sinclair0/sinclair2',
            '104.198.2.82' => 'sinclair1',
            '104.199.114.220' => 'sinclair3',
            '104.197.135.66' => 'sinclair3', // Comet/Charge Affiliate sites are on a shared server, but they show up in sinclair3.
            '35.203.142.3' => 'sinclair4',
            '35.199.186.26' => 'sinclair5',

            // New servers
            '35.225.175.237' => 'compulseweb', // Used to be the combined server.
            '35.184.226.240' => 'compulse1',
            '34.74.188.198'  => 'compulse2',
            '35.196.250.249' => 'compulse3',
            '104.196.40.7'   => 'compulse4',
            '35.229.54.236'  => 'compulse5',
            '104.198.57.224' => 'compulse6',
            '35.224.126.81'  => 'compulse7',
            '34.72.14.41'    => 'compulse8'
        ];

        $server = '';

        if ( array_key_exists($first_site_data['info']['primary_ip'], $ip_to_server) ) {
            $server = $ip_to_server[ $first_site_data['info']['primary_ip'] ];
        }

        $title = '';

        $matches = array();
        if ( preg_match('/<title>([^<]+?)<\/title>/', $last_site_data['body'], $matches) ) {
            $title = $matches[1];
        }

        $auditor_meta_data = $auditor->get_meta_data();

        $new_meta = [
            'install_name' => $site,
            'domain' => $last_site_data['url'],
            'site_title' => $title,
            'results' => $result_arrays,
            'server' => $server,
            'elapsed_time' => $auditor_meta_data['elapsed_time']
        ];

        if ( isset( $auditor_meta_data['disk'] ) ) {
            $new_meta['disk_total'] = $auditor_meta_data['disk']['total'];
            $new_meta['disk_uploads'] = $auditor_meta_data['disk']['uploads'];
        }

        $active_plugin_names = [];

        if ( isset( $auditor_meta_data['active_plugins'] ) && is_array( $auditor_meta_data['active_plugins'] ) ) {
            $new_meta['active_plugins'] = [];

            foreach ( $auditor_meta_data['active_plugins'] as $plugin ) {
                $new_meta['active_plugins'][ $plugin['name'] ] = $plugin['version'];
                $active_plugin_names[] = $plugin['name'];
            }
        }

        // var_dump( $new_meta );

        $result_id;

        // Try to find an existing audit result for this site, and update it.
        $existing_result = get_posts([
            'post_type' => 'auditor_result',
            'post_status' => 'all',
            'meta_query' => [
                [
                    'key' => 'install_name',
                    'value' => $site
                ]
            ],
            'fields' => 'ids'
        ]);

        if ( !empty($existing_result) ) {
            $result_id = $existing_result[0];

            foreach ($new_meta as $key => $value) {
                update_post_meta($result_id, $key, $value);
            }

            // Update time.
            $new_time = current_time('mysql');
            wp_update_post(array(
                'ID' => $result_id,
                'post_date' => $new_time,
                'post_date_gmt' => get_gmt_from_date( $new_time )
            ));
        } else {
            $result_id = wp_insert_post([
                'post_title' => $site,
                'post_type' => 'auditor_result',
                'meta_input' => $new_meta,
                'post_status' => 'private'
            ]);
        }

        // Update active plugin meta values.
        if ( $result_id && !empty( $active_plugin_names ) ) {
            delete_post_meta( $result_id, 'active_plugin_names' );

            foreach ( $active_plugin_names as $name ) {
                add_post_meta( $result_id, 'active_plugin_names', $name, false );
            }
        }

        // var_dump( $result_id );

        return $result_id;
    }

    /**
     * Retrieve the table row HTML for an auditor result.
     * @param int $result_id The auditor_result post ID.
     * @return string
     */
    public function get_auditor_table_row($result_id) {
        $data = get_post_meta($result_id);
        $date = get_the_date('m/d/y', $result_id) . '<br />' . get_the_date('h:ia', $result_id);
        $server = array_key_exists('server', $data) ? $data['server'][0] : '';

        //var_dump($result_id);

        $install_name = $data['install_name'][0];

        $domain = '';
        $last_url = $data['domain'][0];

        $matches = array();
        if ( preg_match('/^https?:\/\/([^\/]+)/', $last_url, $matches) ) {
            $domain = $matches[1];
        }

        $title = $data['site_title'][0];

        $results = maybe_unserialize( $data['results'][0] );

        $ignored_sites = $this->get_ignored_sites_array();

        $lowest_priority_status = "passed";
        $lowest_priority = 20;
        $status_priorities = [
            'passed' => 20,
            'info' => 10,
            'warning' => 5,
            'error' => 1
        ];

        foreach ($results as $test => $result) {
            if ( !empty($result) ) {
                $priority = $status_priorities[ $result['status'] ];
                if ($priority < $lowest_priority) {
                    $lowest_priority = $priority;
                    $lowest_priority_status = $result['status'];
                }
            }
        }

        $active_plugins = [];

        if ( isset( $data['active_plugins'][0] ) ) {
            $active_plugins_data = maybe_unserialize( $data['active_plugins'][0] );

            if ( is_array( $active_plugins_data ) && !empty( $active_plugins_data ) ) {
                $active_plugins = $active_plugins_data;
            }
        }

        $row  = '<tr class="" data-install-name="' . $install_name . '" data-active-plugins="' . esc_attr( json_encode( $active_plugins ) ) . '">';
            $row .= sprintf('<td><a href="http://%1$s.wpengine.com" target="_blank">%1$s</a> <a href="https://my.wpengine.com/installs/%1$s" target="_blank"><small><i class="fa fa-cog"></i></small></a></td>', $install_name);
            $row .= '<td>' . $server . '</td>';
            $row .= '<td><a href="' . $last_url . '" target="_blank">' . $domain . '</a></td>';
            $short_title = (strlen($title) > 50) ? substr($title, 0, 50) . '...' : $title;
            $row .= '<td><div title="' . $title . '">' . $short_title . '</div></td>';

            $info_title = '';

            if ( isset( $data['elapsed_time'][0] ) ) {
                $info_title .= 'Last audit completed in ' . round( $data['elapsed_time'][0], 2 ) . ' seconds.<br /><br />';
            }

            if ( !empty( $active_plugins ) ) {
                $info_title .= 'Active plugins as of last audit:<br />';

                $plugin_strings = [];
                foreach ( $active_plugins as $name => $version ) {
                    $plugin_strings[] = $name . ' (' . $version . ')';
                }

                $plugin_list = implode( ', ', $plugin_strings );
                $info_title .= $plugin_list;
            }

            if ( !empty( $info_title ) ) {
                $row .= '<td title="' . $info_title . '">' . $date . ' <i class="fa fa-info-circle"></i></td>';
            } else {
                $row .= '<td>' . $date . '</td>';
            }

            $order = ['disk','enabled','launched','domain','search','analytics','core','ssl','miscellaneous'];
            $status_to_icon = [
                'passed' => 'check-circle',
                'info' => 'info-circle',
                'warning' => 'exclamation-circle',
                'error' => 'times-circle'
            ];

            //var_dump($data, unserialize($data['results'][0])); exit;

            $most_important_status = 'passed';
            $most_important_status_priority = 20;

            $status_columns = '';

            foreach ($order as $curr) {
                $curr_result = isset($results[$curr]) ? $results[$curr] : [];

                $ignored_statuses = [];
                if ( isset($ignored_sites[ $install_name ]) && isset($ignored_sites[ $install_name ][ $curr ]) && !empty($ignored_sites[ $install_name ][ $curr ]) ) {
                    $ignored_statuses = explode(',', $ignored_sites[ $install_name ][ $curr ]);
                }

                if ( empty($curr_result) || empty($curr_result['messages']) ) {
                    $status_columns .= '<td class="blank"><i class="fa status-icon fa-question-circle"></i></td>';
                } else {
                    if ( $status_priorities[ $curr_result['status'] ] < $most_important_status_priority ) {
                        $most_important_status = $curr_result['status'];
                        $most_important_status_priority = $status_priorities[ $curr_result['status'] ];
                    }

                    $messages = wp_list_pluck($curr_result['messages'], 'message');

                    $column_class = $curr_result['status'];

                    if ( in_array($curr_result['status'], $ignored_statuses) ) {
                        $column_class .= ' ignored';

                        $ignore_note = $ignored_sites[ $install_name ][ 'note' ];

                        $messages[] = 'This ' . $curr_result['status'] . ' is being ignored.<br />Note: ' . $ignore_note;
                    }

                    $column_title = implode('<br /><br />', $messages);

                    $status_columns .= '<td class="' . $column_class . '" title="' . $column_title . '">';
                    $status_columns .= '<i class="fa status-icon fa-';
                    $status_columns .= $status_to_icon[ $curr_result['status'] ];
                    $status_columns .= '" data-priority="' . $status_priorities[ $curr_result['status'] ] . '"></i>';
                    $status_columns .= '</td>';
                }
            }

            // Overall status
            $row .= '<td title="This is the overall status of the site based on the individual statuses to the right." class="' . $most_important_status . '">';
            $row .= '<i class="fa status-icon fa-' . $status_to_icon[ $most_important_status ] . '" data-priority="' . $status_priorities[ $most_important_status ] . '"></i>';
            $row .= '</td>';

            // Other statuses
            $row .= $status_columns;

            //$row .= '<td><a href="#" title="View More Info" class="view-more-info"><i class="fa fa-caret-down"></i></a></td>';
            $row .= '<td><a href="#" title="Run Audit Now" class="run-audit-now"><i class="fa fa-refresh"></i></a></td>';
        $row .= '</tr>';

        return $row;
    }

    /**
     * Handle the [auditor_table] shortcode.
     */
    public function auditor_table_shortcode() {
        ob_start();
        include COMPULSE_SITE_AUDITOR_TEMPLATE_PATH . 'auditor-table-shortcode.php';
        return ob_get_clean();
    }

    /**
     * Handle the [auditor_statistics] shortcode.
     */
    public function auditor_statistics_shortcode() {
        ob_start();
        include COMPULSE_SITE_AUDITOR_TEMPLATE_PATH . 'auditor-statistics-shortcode.php';
        return ob_get_clean();
    }
}



new CompulseSiteAuditorPlugin();
