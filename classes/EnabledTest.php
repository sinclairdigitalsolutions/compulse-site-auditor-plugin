<?php

class EnabledTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('enabled');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        $site_data = $auditor->get_site_data();
        $num_site_data = count($site_data);
        $last_site_data = $site_data[ $num_site_data - 1 ];

        if ( $site_data[0]['info']['http_code'] == 401 ) {
            $result->add_message('Site has been turned off in WP Engine. It may need to be archived.', 'warning');
        } elseif ( $site_data[0]['info']['http_code'] == 404 ) {
            if ( array_key_exists('X-Cache', $site_data[0]['headers']) ) {
                $result->add_message('Site is down for an unknown reason.', 'error');
            } else {
                // This is most likely WPEngine's 404 when an install has been deleted.
                $result->add_message('Site has most likely been deleted in WP Engine. It may need to be removed from the auditor list.', 'warning');
            }
        } elseif ( $site_data[0]['info']['http_code'] >= 500 ) {
            if ( $site_data[0]['info']['http_code'] == 503 &&
                (
                    strpos( $last_site_data['body'], 'wp-content/plugins/under-construction-page' ) !== FALSE  ||
                    strpos( $last_site_data['body'], 'wp-content/plugins/maintenance' ) !== FALSE ||
                    strpos( $last_site_data['body'], 'wp-content/plugins/coming-soon' ) !== FALSE
                )
            ) {
                $result->add_message('Site is using an Under Construction plugin to block access to the site.', 'info');
            } else {
                $result->add_message('Site returned internal server error code ' . $site_data[0]['info']['http_code'] . '.', 'error');
            }
        } else {
            $result->add_message('Site appears to be enabled and working.', 'passed');
        }

        return $result;
    }
}
