<?php

/**
 * Utilities used throughout the site auditor.
 */
final class SiteAuditorUtils {
    private static $logger = null;

    public static function set_logger( SiteAuditorLogger $logger ) {
        self::$logger = $logger;
    }

    public static function get_url_data($url) {
        if ( self::$logger ) {
            self::$logger->log( 'Retrieving data for URL: ' . $url );
            self::$logger->start_elapsed_time();
        }

        $data = [
            'url' => $url,
            'info' => [],
            'headers' => [],
            'body' => ''
        ];

        if ( self::$logger ) {
            self::$logger->log( 'Initializing CURL.' );
        }
        $c = curl_init();
        if ( self::$logger ) {
            self::$logger->log( 'Finished initializing CURL.' );
        }

        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 1,
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => 30
        ]);

        if ( self::$logger ) {
            self::$logger->log( 'Executing CURL.' );
        }
        $body_with_headers = curl_exec($c);
        if ( self::$logger ) {
            self::$logger->log( 'Finished executing CURL.' );
        }

        $data['info'] = curl_getinfo($c);
        $data['body'] = trim( substr($body_with_headers, $data['info']['header_size']) );

        $all_headers = trim( substr($body_with_headers, 0, $data['info']['header_size']) );
        $header_lines = explode("\r\n", $all_headers);

        foreach ( array_slice($header_lines, 1) as $header ) {
            $header_colon_pos = strpos($header, ':');
            $header_name = substr($header, 0, $header_colon_pos);
            $header_value = trim( substr($header, $header_colon_pos + 1) );
            $data['headers'][ $header_name ] = $header_value;
        }

        curl_close($c);

        if ( self::$logger ) {
            self::$logger->log_elapsed_time( 'Finished retrieving URL data.' );
            self::$logger->stop_elapsed_time();
        }

        return $data;
    }

    public static function get_url_data_with_redirects($url) {
        $url_data = array();

        $count = 0;
        $result_code = 0;

        do {
            $data = SiteAuditorUtils::get_url_data( $url );
            $url_data[] = $data;

            $result_code = $data['info']['http_code'];
            $count++;

            if ( array_key_exists('Location', $data['headers']) ) {
                $url = $data['headers']['Location'];
            } else {
                break;
            }
        } while ( in_array($result_code, [301, 302]) && $count < 10  );

        return $url_data;
    }

    private static function get_cloudflare_api_result($url, $raw = false) {
        $cloudflare_api_key = '54da6918fd7b0e025222de1cff7d6d63b4537';
        $cloudflare_email = 'solutions@sinclairdigital.com';

        $c = curl_init();
        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array(
                'X-Auth-Key: ' . $cloudflare_api_key,
                'X-Auth-Email: ' . $cloudflare_email
            )
        ]);
        $result = curl_exec($c);
        $info = curl_getinfo( $c );

        // Check status and throw exception if incorrect.
        if ( curl_errno( $c ) || ($info['http_code'] ?? 0) != 200 ) {
            throw new Exception( 'Failed to retrieve Cloudflare API result.' );
        }

        return $raw ? $result : json_decode($result, true);
    }

    public static function get_base_domain($domain, $include_subdomain = false) {
        $base_domain = $domain;

        $matches = [];
        if ( preg_match('/(https?:\/\/)?([^\/]+)\/?/', $domain, $matches) ) {
            $domain = $matches[2];

            // Remove trailing slash.
            $domain = rtrim($domain, " /\t\n\r\0\x0B\\");

            if ( $include_subdomain ) {
                $base_domain = $domain;
            } else {
                $domain_pieces = explode('.', $domain);
                $num_domain_pieces = count($domain_pieces);
                $base_domain = $domain_pieces[ $num_domain_pieces - 2 ] . '.' . $domain_pieces[ $num_domain_pieces - 1 ];
            }
        }

        return $base_domain;
    }

    public static function get_cloudflare_zone_info($base_domain) {
        $zone_info = self::get_cloudflare_api_result('https://api.cloudflare.com/client/v4/zones?name=' . $base_domain);
        return $zone_info;
    }

    /**
     * @param $base_domain
     * @return array|mixed
     * @throws Exception If failed to retrieve zone info/DNS records due to an API error.
     */
    public static function get_cloudflare_dns_records($base_domain) {
        if ( self::$logger ) {
            self::$logger->log( 'Retrieving Cloudflare DNS records for URL: ' . $base_domain );
            self::$logger->start_elapsed_time();
        }

        $zone_info = self::get_cloudflare_zone_info($base_domain);

        $dns_records = [];

        if ( !empty($zone_info) && !empty($zone_info['result']) ) {
            $zone_id = $zone_info['result'][0]['id'];
            $result = self::get_cloudflare_api_result('https://api.cloudflare.com/client/v4/zones/' . $zone_id . '/dns_records?per_page=100');
            $dns_records = $result['result'];
        }

        if ( self::$logger ) {
            self::$logger->log_elapsed_time( 'Finished retrieving Cloudflare DNS records.' );
            self::$logger->stop_elapsed_time();
        }

        return $dns_records;
    }

    public static function format_kb( $kb ) {
        if ( $kb <= 4 ) {
            return '< 4 KB';
        } else {
            $units = [
                'KB' => 1,
                'MB' => 1024,
                'GB' => 1048576,
                'TB' => 1073741824,
                'PB' => 1099511627776
            ];
            $unit_keys = array_keys( $units );

            for ( $i = 1; $i < count( $unit_keys ); $i++ ) {
                if ( $units[ $unit_keys[ $i ] ] > $kb ) {
                    return round( $kb / $units[ $unit_keys[ $i - 1 ] ], 2 ) . ' ' . $unit_keys[ $i - 1 ];
                }
            }

            return '> 1 PB';
        }
    }
}
















////////////////
