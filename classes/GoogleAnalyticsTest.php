<?php

class GoogleAnalyticsTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('analytics');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        $enabled = ( $auditor->get_test_result('enabled')->get_status() == 'passed' );
        $launched = ( $auditor->get_test_result('launched')->get_status() == 'passed' );
        $correct_domain = ( $auditor->get_test_result('domain')->get_status() != 'error' );

        if ( $enabled && $launched && $correct_domain ) {
            $site_data = $auditor->get_site_data();
            $last_site_data = $site_data[ count($site_data) - 1 ];

            // Check if site has a Google Analytics code installed on it.
            $matches = array();

            if ( preg_match( '/(__gaTracker|ga)\(\'create\'\,\s*\'([^\']+?)\'\,\s*\'auto\'/', $last_site_data['body'], $matches ) ) {
                $result->add_message( 'Site is using Google Analytics code ' . $matches[2] . '.', 'passed' );
            } elseif ( preg_match( '/gtag\(\'config\'\,\s*\'([^\']+?)\'\)/', $last_site_data['body'], $matches ) ) {
                $result->add_message( 'Site is using Google Analytics code ' . $matches[1] . '.', 'passed' );
            } elseif ( preg_match( '/_gaq\.push\(\[\'_setAccount\'\,\s*\'([^\']+?)\'\]\)/', $last_site_data['body'], $matches ) ) {
                $result->add_message( 'Site is using Google Analytics code ' . $matches[1] . '.', 'passed' );
            } elseif ( preg_match( '/gtm.js[\s\S]+?\'(GTM\-[^\']+?)\'/', $last_site_data['body'], $matches ) ) {
                $result->add_message( 'Site is using GTM tag with code ' . $matches[1] . ' instead of Analytics.', 'passed' );
            } else {
                $result->add_message('Site is launched, but no Google Analytics/GTM code was found.', 'warning');
            }
        }

        return $result;
    }
}
