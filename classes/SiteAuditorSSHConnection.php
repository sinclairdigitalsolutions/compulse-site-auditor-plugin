<?php

/**
 * Handles SSH connections and running commands.
 */
class SiteAuditorSSHConnection {
    private $auditor;

    /**
     * @var \phpseclib\Net\SSH2
     */
    private $connection;

    public function __construct( SiteAuditor $auditor ) {
        $this->auditor = $auditor;
        $this->connection = null;
    }

    public function is_connected() {
        return !empty( $this->connection );
    }

    public function disconnect() {
        if ( $this->is_connected() ) {
            $this->connection->disconnect();
        }
    }

    private function get_prompt() {
        // Look for new line then prompt, because WP Engine's SSH output overlaps sometimes.
        return "\n" . 'wpe-user@' . $this->auditor->get_install_name() . '.ssh.wpengine.net:~$';
        //return '';
    }

    private function ensure_connection() {
        if ( $this->connection === null ) {
            $logger = $this->auditor->get_logger();

            if ( !is_readable( COMPULSE_AUDIT_DIR . '/sshkey.php' ) ) {
                $logger->log( 'Cannot set up SSH connection because SSH key file does not exist.' );
            } else {
                // Try to connect.
                include_once COMPULSE_AUDIT_DIR . '/vendor/autoload.php';

                $logger->start_elapsed_time();

                $logger->log( 'Setting up SSH connection.' );

                $ssh = new phpseclib\Net\SSH2( $this->auditor->get_install_name() . '.ssh.wpengine.net' );
                $key = new phpseclib\Crypt\RSA();
                $key->setPassword( 'xd7B2631Gp@VqmQqWD9Njq6$pc^CSpp!' );
                $key->loadKey( include COMPULSE_AUDIT_DIR . '/sshkey.php' );
                $logger->log_elapsed_time( 'Finished setting up SSH connection.', true );

                $logger->log( 'Logging in to SSH connection.' );
                if ( $ssh->login( $this->auditor->get_install_name(), $key ) ) {
                    $this->connection = $ssh;
                    $logger->log_elapsed_time( 'Successfully logged in to SSH connection.', true );

                    $this->connection->setTimeout( 10 );

                    // Skip MOTD
                    $logger->log( 'Skipping MOTD.' );
                    $this->connection->read( $this->get_prompt() );
                    $logger->log_elapsed_time( 'Finished skipping MOTD.' );
                } else {
                    $this->connection = false;
                    $logger->log_elapsed_time( 'Failed to log in to SSH connection.' );
                }

                $logger->stop_elapsed_time();
            }
        }

        return $this->is_connected();
    }

    /**
     * Send a command and return the result lines.
     * @param string $command
     * @return array|bool The lines of the result or false if the command wasn't sent.
     */
    public function send_command( $command ) {
        $result = false;

        if ( $this->ensure_connection() ) {
            $logger = $this->auditor->get_logger();
            $logger->start_elapsed_time();

            $logger->log( 'Writing SSH command: ' . $command );

            // Write and read a blank command to clear out any leftover output.
            $this->connection->write( "\n" );
            $this->connection->read( $this->get_prompt() );

            $this->connection->write( $command . "\n" );
            $logger->log_elapsed_time( 'Finished writing SSH command.', true );

            $logger->log( 'Waiting to read SSH command output.' );
            $read = $this->connection->read( $this->get_prompt() );
            $logger->log_elapsed_time( 'Finished reading SSH command output.' );
            $logger->log( 'Output: ' . $read );

            $read_lines = explode( "\n", $read );

            // Skip this command's input and next prompt line.
            $result = array_slice( $read_lines, 1, -1 );
            array_walk( $result, 'trim' );

            $logger->stop_elapsed_time();
        }

        return $result;
    }
}
