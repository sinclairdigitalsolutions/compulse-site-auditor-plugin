<?php

class CoreVersionTest extends SiteAuditTest {
    private static $latest_wp_version = null;

    public static function get_latest_wp_version() {
        if ( empty(self::$latest_wp_version) ) {
            $latest_version_data = SiteAuditorUtils::get_url_data('https://api.wordpress.org/core/version-check/1.7/');
            $latest_version_info = json_decode($latest_version_data['body'], true);
            self::$latest_wp_version = $latest_version_info['offers'][0]['version'];
        }

        return self::$latest_wp_version;
    }

    public function __construct() {
        parent::__construct('core');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        $enabled = ( $auditor->get_test_result('enabled')->get_status() == 'passed' );
        $correct_domain = ( $auditor->get_test_result('domain')->get_status() != 'error' );

        if ( $enabled && $correct_domain ) {
            // Check WordPress core version.
            $feed_url = $auditor->get_wp_engine_url() . '/?feed=rss2';

            $all_feed_data = SiteAuditorUtils::get_url_data_with_redirects($feed_url);

            $feed_data = $all_feed_data[ count($all_feed_data) - 1 ];
            $generator_matches = array();

            if ( preg_match('/<generator>https?:\/\/wordpress\.org\/\?v=([^<]+?)<\/generator>/', $feed_data['body'], $generator_matches) ) {
                $wordpress_version = $generator_matches[1];

                if ( version_compare($wordpress_version, self::get_latest_wp_version()) < 0 ) {
                    $result->add_message('WordPress core is out of date.  Currently has ' . $wordpress_version . ' (latest is ' . self::$latest_wp_version . ').', 'info');
                } else {
                    $result->add_message('WordPress core is up to date (' . $wordpress_version . ')', 'passed');
                }
            } else {
                $result->add_message('WordPress core version could not be detected by the auditor.  This is not an error.', 'info');
            }
        } else {
            $auditor->get_logger()->log('Skipping core version test because site is either not enabled or there was an error with the domain.');
        }

        return $result;
    }
}
