<?php

class SearchEngineVisibilityTest extends SiteAuditTest {
    public function __construct() {
        parent::__construct('search');
    }

    public function run(SiteAuditor $auditor) {
        $result = parent::run($auditor);

        // Check if site has valid robots.txt.
        $enabled = ( $auditor->get_test_result('enabled')->get_status() == 'passed' );
        $launched = ( $auditor->get_test_result('launched')->get_status() == 'passed' );
        $domain_correct = ( $auditor->get_test_result('domain')->get_status() != 'error' );

        if ( $enabled && (!$launched || $domain_correct) ) {
            $site_data = $auditor->get_site_data();
            $last_site_data = $site_data[ count($site_data) - 1 ];

            $last_site_domain = '';
            $matches = array();
            if ( preg_match('/https?:\/\/[^\/]+/', $last_site_data['url'], $matches) ) {
                $last_site_domain = $matches[0];

                $robots_txt_url = $last_site_domain . '/robots.txt';
                $robots_txt_data = SiteAuditorUtils::get_url_data_with_redirects( $robots_txt_url );

                $last_data = $robots_txt_data[ count($robots_txt_data) - 1 ];

                $robots_txt_body = trim($last_data['body']);
                $body_lines = explode("\n", $robots_txt_body);

                $in_matching_rule = false;
                $blocking_all = false;

                foreach ( $body_lines as $line ) {
                    if ( $line == 'User-agent: *' ) {
                        $in_matching_rule = true;
                    } elseif ( $in_matching_rule ) {
                        //var_dump( substr($line, 0, 9) );
                        if ( substr($line, 0, 9) == 'Disallow:' ) {
                            $disallowed_path = trim( substr($line, 9) );
                            //var_dump($disallowed_path);
                            if ( $disallowed_path == '/' ) {
                                $blocking_all = true;
                                break;
                            }
                        }
                    }
                }

                if ( $blocking_all ) {
                    // Blocking ALL.
                    if ($launched) {
                        $result->add_message('Site is launched, but search engines are disallowed from indexing the site.', 'warning');
                    } else {
                        $result->add_message('Site is not launched and search engine visibility is disabled.', 'passed');
                    }
                } else {
                    // Not blocking all.
                    if ($launched) {
                        $result->add_message('Site is launched and search engine visibility is enabled.', 'passed');
                    } else {
                        $result->add_message('Site is not launched, but search engines are allowed to index the site.', 'warning');
                    }
                }
            }
        } else {
            $auditor->get_logger()->log( 'Skipping search engine visibility test because site is not enabled or the domain is not correct.' );
        }

        return $result;
    }
}
